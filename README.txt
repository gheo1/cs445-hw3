Spring 2020 CS445 HW3
Gyucheon Heo
A20393055

1. Install openjdk-8-default and maven
$>sudo apt-get update -y && sudo apt-get install openjdk-8-jdk maven git

2. Clone the repository
$>git clone http://bitbucket.org/gheo1/cs445-hw3

3. Change the directory, where pom.xml exists
$>cd cs445-hw3/

4. Install dependencies
$>mvn clean install

5. Run Unit Test
$>mvn test

6. Run Main(TableLamp) 
$>mvn exec:java

Memo
1. Lines of code : 70 
2. Lines of code in unit tests :70 
3. Unit test coverage :
Button 100%
PushdownButton 100%
Lightbulb 100%

cyclomatic complexity for code : 
Button.PushdownButton.pushButton() = 2
Anything else = 1

