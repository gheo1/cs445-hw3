package Lightbulb;
import Interactor.LightbulbInterface;

public class Lightbulb implements LightbulbInterface {
    private String status;
    public Lightbulb(){
        status = "off";
    }
    public void on(){
        status = "on";
        System.out.println("Lightbulb on.");
    }
    public void off(){
        status = "off";
        System.out.println("Lightbulb off.");
    }
    public String getStatus(){
        return this.status;
    }
}
