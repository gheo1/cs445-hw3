import Button.Button;
import Button.PushdownButton;
import Interactor.LightbulbInterface;
import Lightbulb.Lightbulb;
public class Main {

    public static void main(String[] args){
        Button b = new Button(new Lightbulb());
        b.switchOn();
        b.switchOff();

//        TableLamp old_tablelamp = new TableLamp(b, new Lightbulb());
        PushdownButton pb = new PushdownButton(new Lightbulb());
        pb.pushButton();
        pb.pushButton();

        TableLamp new_tablelamp = new TableLamp(pb, new Lightbulb());

    }
    public static class TableLamp{
        PushdownButton b;
        LightbulbInterface lbi;
        public TableLamp(PushdownButton b, LightbulbInterface lbi){
            this.b = b;
            this.lbi = lbi;
        }
    }
}

