package Interactor;

public interface LightbulbInterface {
    void on();
    void off();
    String getStatus();
}
