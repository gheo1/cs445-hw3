package Button;

import Interactor.LightbulbInterface;

public class Button {
    private LightbulbInterface lb;
    public Button(LightbulbInterface lb){
        this.lb = lb;
    }
    public void switchOn() {
        lb.on();
        System.out.println("Button switched to ON");
    }

    public void switchOff() {
        lb.off();
        System.out.println("Button switched to OFF");
    }
}
