package Button;

import Interactor.LightbulbInterface;

public class PushdownButton {
    private LightbulbInterface lb;
    public PushdownButton(LightbulbInterface lb){
        this.lb = lb;
    }
    public void pushButton(){
        if(lb.getStatus().equals("on")){
            lb.off();
        } else {
            lb.on();
        }
        System.out.println("Pushed Button");
    }
}
