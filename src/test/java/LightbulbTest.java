import Lightbulb.Lightbulb;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class LightbulbTest {
    private Lightbulb lb;
    private ByteArrayOutputStream outContent;

    @Before
    public void setUp(){
        lb = new Lightbulb();
        outContent = new ByteArrayOutputStream();
    }
    @Test
    public void WhenLightbulbIsCreated_status_should_be_off(){
        assertEquals("off", lb.getStatus());
    }
    @Test
    public void WhenTurnOn_Lightbulb_rpint_Lightbulb_on(){
        System.setOut(new PrintStream(outContent));
        lb.on();
        assertEquals("Lightbulb on.\n", outContent.toString());
        assertEquals("on", lb.getStatus());
    }
    @Test
    public void WhenTurnOff_Lightbulb_print_Lightbulb_off(){
        System.setOut(new PrintStream(outContent));
        lb.off();
        assertEquals("Lightbulb off.\n", outContent.toString());
        assertEquals("off", lb.getStatus());
    }


}
