import Button.Button;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import Lightbulb.Lightbulb;
import static org.junit.Assert.assertEquals;

public class ButtonTest {
    private Button b;
    private ByteArrayOutputStream outContent;
    @Before
    public void setUp(){
        b = new Button(new Lightbulb());
        outContent = new ByteArrayOutputStream();
    }

    @Test
    public void ButtonSwithOn_print_Button_switched_to_ON(){
        System.setOut(new PrintStream(outContent));
        b.switchOn();
        assertEquals("Lightbulb on.\nButton switched to ON\n", outContent.toString());
    }
    @Test
    public void ButtonSwithOn_print_Button_switched_to_OFF(){
        System.setOut(new PrintStream(outContent));
        b.switchOff();
        assertEquals("Lightbulb off.\nButton switched to OFF\n", outContent.toString());
    }
}
