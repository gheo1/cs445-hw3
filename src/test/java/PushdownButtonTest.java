import Button.PushdownButton;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import Lightbulb.Lightbulb;
import static org.junit.Assert.assertEquals;

public class PushdownButtonTest {
    private PushdownButton button;
    private ByteArrayOutputStream outContent;
    @Before
    public void setUp(){
        button = new PushdownButton(new Lightbulb());
        outContent  = new ByteArrayOutputStream();
    }
    @Test
    public void pushdownButton_should_turn_on_if_lightbulb_is_off(){
        System.setOut(new PrintStream(outContent));
        button.pushButton();
        assertEquals("Lightbulb on.\nPushed Button\n", outContent.toString());
    }

    @Test
    public void pushdownButton_should_turn_off_if_lightbulb_is_on(){
        button.pushButton();
        System.setOut(new PrintStream(outContent));
        button.pushButton();
        assertEquals("Lightbulb off.\nPushed Button\n", outContent.toString());

    }
}
